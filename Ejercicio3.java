package modulo1;

public class Ejercicio3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Tecla de escape \t\t Significado"); 
		System.out.println ("\\n \t \t \t \t Significa nueva l�nea");
		System.out.println ("\\t \t \t \t \t Significa un tab de separaci�n");
		System.out.println ("\\\" \t \t \t \t Es para poner \" (Comillas dobles)dentro del texto como por ejemplo \"Belencita\"");
		System.out.println ("\\\\ \t \t \t \t Se utiliza para escribir la \\ por ejemplo \\algo\\");
		System.out.println ("\\\' \t \t \t \t Se utiliza para escribir las \' (comillas simples) como por ejemplo \'Bellaquita\' ");
	}
	

}
